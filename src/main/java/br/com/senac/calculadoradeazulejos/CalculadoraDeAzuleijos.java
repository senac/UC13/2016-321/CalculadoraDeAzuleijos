package br.com.senac.calculadoradeazulejos;

public class CalculadoraDeAzuleijos {

    private double AZULEIJO = 1.5;

    public int calcular(Comodo comodo) {

        double area = comodo.getAreaTotal();
        int caixas = (int) Math.ceil(area / AZULEIJO);
        return caixas;
    }

}
