
package br.com.senac.calculadoradeazulejos;

import static org.junit.Assert.*;
import org.junit.Test;


public class CalculadoraDeAzuleijosTest {
    
    @Test
    public void deveCalcular1CaixasParaUmaAreaDe1MetrosQuadrados(){
        ComodoRegular cozinha = new ComodoRegular(1, 0.5, 0.5); 
        CalculadoraDeAzuleijos calculadoraDeAzuleijos = new CalculadoraDeAzuleijos() ; 
        double caixas = calculadoraDeAzuleijos.calcular(cozinha);
        assertEquals( 1 , caixas , 0 );
    }
    
    
    @Test
    public void deveCalcular24CaixasParaUmaAreaDe36Quadrados(){
        ComodoRegular cozinha = new ComodoRegular(3.5, 2.5, 3); 
        CalculadoraDeAzuleijos calculadoraDeAzuleijos = new CalculadoraDeAzuleijos() ; 
        double caixas = calculadoraDeAzuleijos.calcular(cozinha);
        assertEquals( 24 , caixas , 0 );
    }
    
    @Test
    public void deveCalcular18CaixasParaUmaAreaDe17eMeioQuadrados(){
        ComodoRegular cozinha = new ComodoRegular(3, 1, 3.3); 
        CalculadoraDeAzuleijos calculadoraDeAzuleijos = new CalculadoraDeAzuleijos() ; 
        double caixas = calculadoraDeAzuleijos.calcular(cozinha);
        assertEquals( 18 , caixas , 0 );
    }
    
}
