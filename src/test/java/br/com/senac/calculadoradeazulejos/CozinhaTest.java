package br.com.senac.calculadoradeazulejos;

import static org.junit.Assert.*;
import org.junit.Test;

public class CozinhaTest {

    @Test
    public void deveCalcularAreaTotalCozinha() {
        ComodoRegular cozinha = new ComodoRegular(1, 0.5, 0.5);
        double area = cozinha.getAreaTotal();
        assertEquals(1.5, area , 0);

    }

}
